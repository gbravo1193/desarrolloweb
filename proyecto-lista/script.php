<?php

function soloLetras($in){
    if(preg_match('/[^a-Z]/',$in)) return false;
    else return true;
}

$usuario = $_POST["user"];
$apellido = $_POST["lastname"];
$edad = $_POST["age"];
$resultado="";
if(empty($usuario)){
    $resultado = $resultado. ", Nombre vacio";
}else{
    if(!ctype_alpha($usuario)){
        $resultado = $resultado. ", Nombre debe contener solo letras";
    }
}
if(empty($apellido)){
    $resultado = $resultado.", Apellido vacio";
}else{
    if(!ctype_alpha($apellido)){
        $resultado = $resultado.", Apellido debe contener solo letras";
    }
}

if(empty($edad)){
    $resultado = $resultado.", Edad vacia";
}

if(empty($resultado)){
    $resultado = "Todo correcto";
}else{
    $resultado = "Se han encontrado errores: ".$resultado;
}
echo $resultado;
/**
$array = array("pedro","homero","bart");
$resultado = "No se ha encontrado en la lista";
for ($i = 0; $i < count($array); $i++) {
    if($usuario == $array[$i]){
        $resultado = "Si se encuentra en la lista";
        break;
    }
}
echo $resultado;
**/
?>