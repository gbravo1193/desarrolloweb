/**
 * Created by Bravo on 15-04-16.
 */

var btn = document.getElementById("btn");

var resultado = document.getElementById("resultado");

var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");
var select = document.getElementById("input-select");

btn.addEventListener("click", function () {
    var n1 = inputUno.value;
    var n2 = inputDos.value;

    if (n1 == "" || n2 == "") {

        resultado.innerHTML = "No esta permitida la division por cero";
        resultado.style.color = "red";

    } else {

        n1 = parseInt(n1);
        n2 = parseInt(n2);
        var seleccion = select.value;


        if (seleccion == 'suma') {
            resultado.innerHTML = sumar(n1, n2);
        }
        if (seleccion == 'resta') {
            resultado.innerHTML = restar(n1, n2);
        }

        if (seleccion == 'mult') {
            resultado.innerHTML = multiplicar(n1, n2);
        }
        if (seleccion == 'div') {
            if (n2 == 0) {

                resultado.innerHTML = "No se puede dividir por cero";
                resultado.style.color = "red";
            } else {
                resultado.innerHTML = dividir(n1, n2);

            }
        }


    }


});

function sumar(n1, n2) {
    return n1 + n2;
}

function restar(n1, n2) {
    return n1 - n2;
}

function multiplicar(n1, n2) {
    return n1 * n2;
}

function dividir(n1, n2) {
    return n1 / n2;
}

function validar() {

    if ((event.keyCode < 48) || (event.keyCode > 57)) {
        event.returnValue = false;
        resultado.innerHTML = "No se permiten valores vacios o letras";
        resultado.style.color = "red";


    } else {
        resultado.innerHTML = "";

    }


}

